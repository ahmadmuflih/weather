package com.baso.weather.network;


import com.baso.weather.model.WeatherResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface NetworkService {

    @GET("v1/forecast.json")
    Observable<WeatherResponse> getWeather(@Query("key") String apiKey,@Query("q") String cityName,@Query("days") int days);

}