package com.baso.weather.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.baso.weather.R;
import com.baso.weather.model.Forecast;
import com.baso.weather.model.Forecastday;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {
    private Context context;
    private List<Forecastday> data;
    private final OnItemClickListener listener;

    public WeatherAdapter(Context context, List<Forecastday> data, OnItemClickListener listener) {
        this.context = context;
        this.data = data;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Forecastday item = data.get(position);


        //Convert given date to day name
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = inFormat.parse(item.getDate());
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE",Locale.ENGLISH);
            String day = outFormat.format(date);
            holder.txt_dayname.setText(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        //Show average temp of the day
        holder.txt_temperature.setText(item.getDay().getAvgtempC()+"º");

        //SHOW ICON IMAGE USING PICASSO
        Picasso.get().load("http://"+item.getDay().getCondition().getIcon().substring(2)).fit().into(holder.img_weather);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public interface OnItemClickListener {
        void onClick(Forecastday Item);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dayname_txt)
        TextView txt_dayname;
        @BindView(R.id.temperature_txt)
        TextView txt_temperature;
        @BindView(R.id.weather_img)
        ImageView img_weather;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);




        }


        public void click(final Forecastday weatherData, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(weatherData);
                }
            });
        }
    }
}
