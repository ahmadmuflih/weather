package com.baso.weather;

import android.app.Application;

import com.baso.weather.utils.FontsOverride;

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/neosansthick.otf");
        //  This FontsOverride comes from the example I posted above
    }
}