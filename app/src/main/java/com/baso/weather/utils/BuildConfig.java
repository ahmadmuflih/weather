package com.baso.weather.utils;

public class BuildConfig {
    public static final int LIMIT = 100;
    public static final String BASEURL = "http://api.apixu.com";
    public static final int CACHETIME = 60;
}
