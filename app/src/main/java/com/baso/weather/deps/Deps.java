package com.baso.weather.deps;


import com.baso.weather.main_activity.MainActivity;
import com.baso.weather.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    void inject(MainActivity mainActivity);
}