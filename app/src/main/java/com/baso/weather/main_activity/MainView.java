package com.baso.weather.main_activity;

import com.baso.weather.model.WeatherResponse;

public interface MainView {
    void showWait();

    void removeWait();

    void onFailure(String appErrorMessage);

    void getWeatherSuccess(WeatherResponse weatherResponse);

}