package com.baso.weather.main_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baso.weather.BaseApp;
import com.baso.weather.R;
import com.baso.weather.adapter.WeatherAdapter;
import com.baso.weather.model.Forecastday;
import com.baso.weather.model.WeatherResponse;
import com.baso.weather.network.Service;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseApp implements MainView {
    @BindView(R.id.location_txt)
    TextView locationTxt;
    @BindView(R.id.condition_txt)
    TextView conditionTxt;
    @BindView(R.id.temperature_txt)
    TextView temperatureTxt;
    @BindView(R.id.lastupdated_txt)
    TextView lastupdated_txt;
    @BindView(R.id.weather_img)
    ImageView weatherImg;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @Inject
    public Service service;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        getData();


    }

    private void getData() {
        MainPresenter presenter = new MainPresenter(service, this);
        presenter.getWeatherList();
        Log.d("MAIN","GET DATA");
    }

    private void init() {
        getSupportActionBar().hide();
        recyclerView.setLayoutManager( new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

    }


    @Override
    public void showWait() {
        swipe.setRefreshing(true);
    }

    @Override
    public void removeWait() {
        swipe.setRefreshing(false);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show();
        swipe.setRefreshing(false);
    }

    @Override
    public void getWeatherSuccess(WeatherResponse weatherResponse) {
        locationTxt.setText(weatherResponse.getLocation().getName());
        temperatureTxt.setText(weatherResponse.getCurrent().getTempC()+"º");
        conditionTxt.setText(weatherResponse.getCurrent().getCondition().getText());
        Picasso.get().load("http://"+weatherResponse.getCurrent().getCondition().getIcon().substring(2)).fit().into(weatherImg);
        //Show last updated data (only hour:minute)
        lastupdated_txt.setText("Last Updated : "+weatherResponse.getCurrent().getLastUpdated().substring(11));

        List<Forecastday> data = weatherResponse.getForecast().getForecastday();
        //remove today's data and only show last 6 yesterdays data
        data.remove(0);
        WeatherAdapter adapter = new WeatherAdapter(getApplicationContext(), data, new WeatherAdapter.OnItemClickListener() {
            @Override
            public void onClick(Forecastday Item) {

            }
        });
        recyclerView.setAdapter(adapter);
    }


}
