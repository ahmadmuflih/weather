package com.baso.weather.network;

import com.baso.weather.model.WeatherResponse;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Service {
    private final NetworkService networkService;
    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getWeather(final GetWeatherCallback callback){
        return networkService.getWeather("2d8ed86157654ff092131816192208","bandung",7)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends WeatherResponse>>() {
                    @Override
                    public Observable<? extends WeatherResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<WeatherResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(WeatherResponse weatherResponse) {
                        callback.onSuccess(weatherResponse);

                    }
                });
    }
    public interface GetWeatherCallback{
        void onSuccess(WeatherResponse weatherResponse);

        void onError(NetworkError networkError);
    }
}
