package com.baso.weather.main_activity;

import com.baso.weather.model.WeatherResponse;
import com.baso.weather.network.NetworkError;
import com.baso.weather.network.Service;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class MainPresenter {
    private final Service service;
    private final MainView view;
    private CompositeSubscription subscriptions;

    public MainPresenter(Service service, MainView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
    }

    public void getWeatherList() {
        view.showWait();

        Subscription subscription = service.getWeather(new Service.GetWeatherCallback() {
            @Override
            public void onSuccess(WeatherResponse weatherResponse) {
                view.removeWait();
                view.getWeatherSuccess(weatherResponse);
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
                view.onFailure(networkError.getAppErrorMessage());
            }
        });

        subscriptions.add(subscription);
    }
    public void onStop() {
        subscriptions.unsubscribe();
    }
}
