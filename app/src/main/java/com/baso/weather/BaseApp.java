package com.baso.weather;

import android.os.Bundle;

import com.baso.weather.deps.DaggerDeps;
import com.baso.weather.deps.Deps;
import com.baso.weather.network.NetworkModule;

import java.io.File;

import androidx.appcompat.app.AppCompatActivity;

public class BaseApp  extends AppCompatActivity {
    Deps deps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();

    }

    public Deps getDeps() {
        return deps;
    }
}